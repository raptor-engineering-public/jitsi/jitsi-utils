Document: jitsi-utils
Title: Debian jitsi-utils Manual
Author: <insert document author here>
Abstract: This manual describes what jitsi-utils is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/jitsi-utils/jitsi-utils.sgml.gz

Format: postscript
Files: /usr/share/doc/jitsi-utils/jitsi-utils.ps.gz

Format: text
Files: /usr/share/doc/jitsi-utils/jitsi-utils.text.gz

Format: HTML
Index: /usr/share/doc/jitsi-utils/html/index.html
Files: /usr/share/doc/jitsi-utils/html/*.html
