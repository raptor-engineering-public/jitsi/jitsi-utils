#
# Regular cron jobs for the jitsi-utils package
#
0 4	* * *	root	[ -x /usr/bin/jitsi-utils_maintenance ] && /usr/bin/jitsi-utils_maintenance
